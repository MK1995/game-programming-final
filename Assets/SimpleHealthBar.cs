﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SimpleHealthBar : MonoBehaviour
{
  public Slider healthbar;
  PlayerController health;

  void Start ()
  {
    health = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
  }

  void Update ()
  {
    healthbar.value = PlayerController.health;
  }

}
