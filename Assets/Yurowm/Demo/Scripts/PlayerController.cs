﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

[RequireComponent (typeof (Animator))]
public class PlayerController : MonoBehaviour {
    CharacterController soild;
    public float speed = 10;
    float gravity = -15f;
    float ySpeed = 0;
    public Transform cam;
    public GameObject pit;
    float pitch = 0f;
    public bool alarm = false;
    public Material scr;
    public GameObject heli;
    public GameObject wav;
    public Transform rightGunBone;
	public Transform leftGunBone;
	public Arsenal[] arsenal;
    public static bool flying = false;
	private Animator animator;
    public static int health = 200;

    void Start()
    {
        heli.SetActive(false);
        wav.GetComponent<WaveSpawner>().enabled = false;
        soild = GetComponent<CharacterController>();
        SetArsenal("Rifle");
        scr.color = Color.green;

    }
    void Update()
    {
        Actions actions = GetComponent<Actions>();

        float xInput = Input.GetAxis("Horizontal")*speed;
        float zInput = Input.GetAxis("Vertical")*speed;

        Vector3 move = new Vector3(xInput, 0, zInput);
        move = Vector3.ClampMagnitude(move,speed);
        move = transform.TransformVector(move);

        if (Input.GetButton("Horizontal") || Input.GetButton("Vertical"))
        {
            actions.Walk();
            //FindObjectOfType<ManagerAudio>().Play("footsteps");
            //haven't figured out where to place this atm
            //currently this would just loop over and over and Play
            //on standing still.
        }
        else
        {
            actions.Stay();
        }

            if (soild.isGrounded)
        {
            if(Input.GetButtonDown("Jump"))
            {
                actions.Jump();
                ySpeed = 6f;
            }
            else
            {
                ySpeed = gravity*Time.deltaTime;
            }
        }
        else
        {
            ySpeed += gravity * Time.deltaTime;

        }

        if (Input.GetButton("Fire2"))
        {
            actions.Aiming();
        }
            //soild.Move(new Vector3(xInput, ySpeed, zInput) * Time.deltaTime);
        soild.Move((move + new Vector3(0, ySpeed, 0)) * Time.deltaTime);

        float xMouse = Input.GetAxis("Mouse X")* 10f;
        transform.Rotate(0,xMouse,0);

        pitch -= Input.GetAxis("Mouse Y")*10f;
        pitch = Mathf.Clamp(pitch, -45f, 45f);
        Quaternion camRo = Quaternion.Euler(pitch,0,0);

        cam.localRotation=camRo;
       if( Input.GetKey("e"))
        {

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward,out hit, 2))
            {
                if(hit.collider.name == "console")
                {
                  //add alarm sounds here
                    heli.SetActive(true);
                    wav.GetComponent<WaveSpawner>().enabled = true;
                    scr.color = Color.red;
                    FindObjectOfType<ManagerAudio>().Play("alarm");
                }
                else if(hit.collider.name == "Heli1")
                {
                    //Debug.Log("in");
                    this.GetComponent<CharacterController>().enabled = false;
                    flying = true;
                    StartCoroutine(pilot());
                }
            }
        }
    }
    public void TakeDamage(int amount)
    {
        Actions actions = GetComponent<Actions>();
        health -= amount;
        actions.Damage();
        if (health <= 0f)
        {

            actions.Death();
            StartCoroutine(Die());


        }
    }
    IEnumerator Die()
    {

        yield return new WaitForSeconds(3);
        health = 200;
        SceneManager.LoadScene(2);
    }
    IEnumerator pilot()
    {
        while (1==1)
        {

            this.transform.position = pit.transform.position;
            yield return null;
        }
    }
	void Awake() {
		animator = GetComponent<Animator> ();
		if (arsenal.Length > 0)
			SetArsenal (arsenal[0].name);
		}

	public void SetArsenal(string name) {
		foreach (Arsenal hand in arsenal) {
			if (hand.name == name) {
				if (rightGunBone.childCount > 0)
					Destroy(rightGunBone.GetChild(0).gameObject);
				if (leftGunBone.childCount > 0)
					Destroy(leftGunBone.GetChild(0).gameObject);
				if (hand.rightGun != null) {
					GameObject newRightGun = (GameObject) Instantiate(hand.rightGun);
					newRightGun.transform.parent = rightGunBone;
					newRightGun.transform.localPosition = Vector3.zero;
					newRightGun.transform.localRotation = Quaternion.Euler(90, 0, 0);
					}
				if (hand.leftGun != null) {
					GameObject newLeftGun = (GameObject) Instantiate(hand.leftGun);
					newLeftGun.transform.parent = leftGunBone;
					newLeftGun.transform.localPosition = Vector3.zero;
					newLeftGun.transform.localRotation = Quaternion.Euler(90, 0, 0);
				}
				animator.runtimeAnimatorController = hand.controller;
				return;
				}
		}
	}



    [System.Serializable]
	public struct Arsenal {
		public string name;
		public GameObject rightGun;
		public GameObject leftGun;
		public RuntimeAnimatorController controller;
	}
}
