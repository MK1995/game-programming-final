﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneFollow : MonoBehaviour
{

    [SerializeField]
    bool _patrolWaiting;

    //The total time we wait at each node.
    [SerializeField]
    float _totalWaitTime ;
    public GameObject heli=null;
    public Transform[] path;
    public float speed ;
    public float reachDist = 1.0f;
    public int currentPoint = 0;

    //Private variables for base behaviour.
    
    int _currentPatrolIndex;
    bool _travelling;
    public bool _waiting;
    bool _patrolForward;
    float _waitTimer;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
       
        float dist = Vector3.Distance(path[currentPoint].position, transform.position);
        
        transform.position = Vector3.Lerp(transform.position,path[currentPoint].position,Time.deltaTime*speed);
        if( currentPoint !=2 )
        transform.LookAt(path[currentPoint].position);
       


        if (dist <= reachDist)
        {
            if (currentPoint==1)
            {
                
                _waiting = true;

                _waitTimer += Time.deltaTime;
                if (_waitTimer >= _totalWaitTime)
                {
                    _waitTimer=0;
                    _waiting = false;
                    currentPoint++;
                }
            }
            else if(currentPoint == 2 && this.transform.name == "Heli1")
            {
                if (currentPoint == 2)
                {

                    _waiting = true;
                    if (PlayerController.flying)
                    {
                        _waitTimer = 0;
                        _waiting = false;
                        currentPoint++;
                        this.GetComponent<PlaneFollow>().enabled = false;
                        this.GetComponent<Extraction>().enabled = true;
                    }
                }
                
            }
            else
                currentPoint++;

        }
        if(currentPoint>=path.Length)
        {
            currentPoint = 0;

        }
    }

    void OnDrawGizmos()
    {
        if (path.Length > 0)
            for (int I = 0; I < path.Length; I++)
        {
            
            if(path[I] !=null)
            {
                Gizmos.DrawSphere(path[I].position,reachDist);

            }
        }

    }
}
