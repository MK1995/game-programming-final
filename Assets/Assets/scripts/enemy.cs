﻿
using UnityEngine;
using System.Collections;
public class enemy : MonoBehaviour
{
   
    public float health=50f;
    public  bool alive=true;

    void Start()
    {
        alive = true;

    }
    public void TakeDamage (float amount)
    {
        alive = true;
        health -= amount;
        if(health <=0f)
        {
           
            Actions actions = GetComponent<Actions>();
            actions.Death();
            StartCoroutine(Die());
            

        }
    }
    IEnumerator Die ()
    {
        yield return new WaitForSeconds(3);
        Destroy(gameObject);
    }
}
