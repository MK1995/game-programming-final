﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Extraction : MonoBehaviour
{
    //scene manager
    SceneManager sceneManager;

    [SerializeField]
    bool _patrolWaiting;

    //The total time we wait at each node.
    [SerializeField]
    float _totalWaitTime;

    public Transform[] path;
    public float speed;
    public float reachDist = 1.0f;
    public int currentPoint = 0;
    public static bool end=false;
    //Private variables for base behaviour.
    public GameObject manager;
    int _currentPatrolIndex;
    bool _travelling;
    public bool _waiting;
    bool _patrolForward;
    float _waitTimer;
    // Start is called before the first frame update
    void Start()
    {
        manager.GetComponent<explode>().enabled = true;
    }

    // Update is called once per frame
    void Update()
    {

        float dist = Vector3.Distance(path[currentPoint].position, transform.position);

        transform.position = Vector3.Lerp(transform.position, path[currentPoint].position, Time.deltaTime * speed);
        if (currentPoint != 0 && currentPoint != 1)
            transform.LookAt(path[currentPoint].position);



        if (dist <= reachDist)
        {
            if (currentPoint == 2)
            {
                end = true;
                _waiting = true;

                _waitTimer += Time.deltaTime;
                if (_waitTimer >= _totalWaitTime)
                {
                    _waitTimer = 0;
                    _waiting = false;
                    currentPoint++;
                }
            }
            else
            {
                currentPoint++;
                //SceneManager.LoadScene(3); //should load scene after explosions but this line loads it too early
            }
        }
        if (currentPoint >= path.Length)
        {
            GetComponent<Extraction>().enabled = false;

        }
    }

    void OnDrawGizmos()
    {
        if (path.Length > 0)
            for (int I = 0; I < path.Length; I++)
            {

                if (path[I] != null)
                {
                    Gizmos.DrawSphere(path[I].position, reachDist);

                }
            }

    }
}


