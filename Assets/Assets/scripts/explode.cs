﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class explode : MonoBehaviour
{
    public float delay=3f;
    public GameObject explosion;
    public List<GameObject>boom;
    float countdown;
    public GameObject plasma;
   
    bool hasExploded = false;
    public GameObject base1boom;
    public GameObject base2boom;
    public GameObject base1;
    public GameObject base2;
    // Start is called before the first frame update
    void Start()
    {
        countdown = delay;
    }

    // Update is called once per frame
    void Update()
    {
        countdown -= Time.deltaTime;
       
        if (countdown <= 0f && boom.Count != 0)
        {
            countdown = 2f;
            Explode();
            
           
        }
        if(Extraction.end && !hasExploded)
        {
            hasExploded = true;
            finale(base1boom,base1);
            finale(base2boom,base2);
        }

    }

    void Explode ()
    {
        int random = Random.Range(0, boom.Count);
       GameObject target = boom[random];
        GameObject explosionObject = Instantiate(explosion, target.transform.position, target.transform.rotation) as GameObject;
        FindObjectOfType<ManagerAudio>().Play("boom");
        Destroy(explosionObject,1.9f);
        boom.Remove(target);
        Destroy(target);
        
    }
   void finale(GameObject target,GameObject base2)
    {
        GameObject explosionObject = Instantiate(plasma, target.transform.position, target.transform.rotation) as GameObject;
        Destroy(explosionObject, 1.9f);
        FindObjectOfType<ManagerAudio>().Play("boom");
        Destroy(base2);
        Invoke("winCondition", 10);
        //SceneManager.LoadScene(3);

    }

    void winCondition()
    {
        SceneManager.LoadScene(3);

    }
    /* IEnumerator Wait()
     {
          yield return new WaitForSeconds(8);

     } */

}
