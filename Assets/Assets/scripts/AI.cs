﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class AI : MonoBehaviour
{
    public float lookRadius = 10f;
    public float range = 100f;
    Transform target;
    NavMeshAgent agent;
    bool found=false;
    public bool GunAim;
         
    // Start is called before the first frame update
    void Start()
    {
        target = PlayerManager.instance.player.transform;
        agent = GetComponent<NavMeshAgent>();
        Actions actions = GetComponent<Actions>();
        if(this.name != "SoldierSniper")
        actions.Walk();
    }

    // Update is called once per frame
    void Update()
    {
       
        Actions actions = GetComponent<Actions>();
        float distance = Vector3.Distance(target.position, transform.position);
        
    Vector3 targetDir = target.position - transform.position;
        float angleToPlayer = (Vector3.Angle(targetDir, transform.forward));
        RaycastHit hit;
        if (angleToPlayer >= -90 && angleToPlayer <= 90) // 180° FOV
        {
          if (Physics.Raycast(transform.position, targetDir, out hit, range))
          {
                if (hit.transform == target)
                {
                    //Can see player
                    if (distance <= lookRadius)
                    {
                        GunAim = true;
                        found = true;
                        actions.Aiming();
                        agent.isStopped = true;
                        FaceTarget();
                    }
                    else if (found == true)
                    {
                        GunAim = false;
                        actions.Walk();
                        agent.isStopped = false;
                        agent.SetDestination(target.position);
                        if (distance <= agent.stoppingDistance)
                        {
                            FaceTarget();

                        }

                    }
                }
                else
                {
                    //lost player
                }
          }
        }
       

    }

    void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }
}
