﻿
using UnityEngine;

public class SHOOT : MonoBehaviour
{
    public float damage = 10f;
    public float range = 100f;
    public camera fps;
    // Start is called before the first frame update
    public ParticleSystem muzzleflash;



    // Update is called once per frame
    void Update()
    {
        if ( Input.GetButton("Fire2") )
        {
            if (Input.GetButtonDown("Fire1"))
            {
                muzzleflash.Play();
                fire();
                FindObjectOfType<ManagerAudio>().Play("shoot");
            }
        }
    }
    void fire()
    {
      muzzleflash.Play();
        RaycastHit hit;
        if(Physics.Raycast(fps.transform.position,fps.transform.forward.normalized,out hit,range))
        {
            Debug.Log(hit.transform.name);
           enemy enemy = hit.transform.GetComponent<enemy>();
            if(enemy != null && enemy.health !=0)
            {

                enemy.TakeDamage(damage);
            }
        }

    }
}
