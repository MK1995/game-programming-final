﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class patrolshoot : MonoBehaviour
{
    public GameObject me;
    public ParticleSystem muzzleflash;
    public float damage = 10f;
    public float range = 100f;
    public bool alive = true;
    public bool aim;
    void Start()
    {

        StartCoroutine(shoot());
    }
    // Update is called once per frame
    void Update()
    {

        alive = me.GetComponent<enemy>().alive;
        aim = me.GetComponent<AI>().GunAim;
    }


    IEnumerator shoot()
    {
        Actions actions = me.GetComponent<Actions>();

        while (alive)
        {

            if (aim == true)
            {

                actions.Attack();
                fire();
                FindObjectOfType<ManagerAudio>().Play("shoot");
            }
            yield return new WaitForSecondsRealtime(1.0f);
        }
    }
    void fire()
    {
        muzzleflash.Play();
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward.normalized, out hit, range))
        {
            Debug.Log(hit.transform.name);
            string name = hit.transform.name;

            if (name == "Sci-Fi_Soldier")
            {
                //PlayerController.health -= 10;
                PlayerController player = hit.transform.GetComponent<PlayerController>();
                player.TakeDamage(10);
            }


        }

    }
}
